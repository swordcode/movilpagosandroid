package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.ServiceInfo;
import com.swordcode.movilpagos.models.Transaction;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.swordcode.movilpagos.constants.Constants.*;
/**
 * Created by euclidesflores on 2/6/14.
 */
public class PaymentHistoryDetailActivity extends Activity
{
	private ServiceInfo								_si;
	private Account									_account;
	private TextView 								_emptyMssg;
	private ListView								_listView;
	private ParseQueryAdapter<ParseObject> 			_adapter;
	private SimpleDateFormat						_sdf = new SimpleDateFormat("d-MMMM-yyyy h:m a", new Locale("es"));
	private NumberFormat							_format = NumberFormat.getCurrencyInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_history_detail_layout);
		_listView = (ListView)findViewById(R.id.transaction_list);
		_emptyMssg = (TextView)findViewById(android.R.id.empty);
		Bundle bundle = getIntent().getBundleExtra("serviceAndAccountBundle");
		_si = (ServiceInfo)bundle.getParcelable("serviceInfo");
		_account = (Account)bundle.getParcelable("account");
		String title = String.format("%s - %s", _account.getAccountID(), _si.getServiceName());
		setTitle(title);
		_emptyMssg.setText("Cargando transacciones");
		setProgressBarIndeterminateVisibility(true);
		ParseQueryAdapter.QueryFactory<ParseObject> factory =
			new ParseQueryAdapter.QueryFactory<ParseObject>() {
				public ParseQuery create() {
					ParseQuery query = new ParseQuery("transactions");
					query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
					query.whereEqualTo("userID", ParseUser.getCurrentUser().getObjectId());
					query.whereEqualTo("accountID", _account.getAccountID());
					return query;
			}
		};

		_adapter = new ParseQueryAdapter<ParseObject>(this, factory) {
			@Override
			public View getItemView(final ParseObject object, final View v, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater) PaymentHistoryDetailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView = inflater.inflate(R.layout.trx_row_layout, parent, false);
				TextView tx1 = (TextView) rowView.findViewById(R.id.col1);
				TextView tx2 = (TextView) rowView.findViewById(R.id.col2);
				Date trxDate = object.getDate("payment_date");
				double amount = object.getNumber("amount").doubleValue();
				tx1.setText(_sdf.format(trxDate));
				tx2.setText(_format.format(amount));
				return rowView;
			}
		};

		_adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
			@Override
			public void onLoading() {
				_emptyMssg.setText("Cargando información de transacciones");
			}

			@Override
			public void onLoaded(List<ParseObject> parseObjects, Exception e) {
				if (parseObjects != null && parseObjects.size() < 1) {
					_emptyMssg.setText(R.string.emptyListTrx);
				} else {
					_emptyMssg.setVisibility(TextView.GONE);
				}
				setProgressBarIndeterminateVisibility(false);
			}
		});

		_listView.setAdapter(_adapter);
		_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				ParseObject o = _adapter.getItem(i);
				Transaction trx = new Transaction(o.getObjectId(), o.getString("userID"),
					o.getString("accountID"), o.getDate("payment_date"), o.getNumber("amount"),
					o.getNumber("balance"), o.getNumber("serviceID"), o.getString("stripePaymentID"),
					o.getString("last4"), o.getString("cardType"), o.getNumber("status"), _si.getServiceName()
				);

				Intent intent = new Intent(view.getContext(), CheckoutCompleteActivity.class);
				Bundle bundle = new Bundle();
				bundle.putParcelable("trx", trx);
				intent.putExtra("trxBundle", bundle);
				intent.putExtra("requestCode", ACTIVITY_CODE_TRX);
				startActivityForResult(intent, ACTIVITY_CODE_TRX);
//				Toast.makeText(getApplicationContext(), o.getString("stripePaymentID"),
//						Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}
