package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.ServiceInfo;

import java.text.NumberFormat;
import java.util.Date;
import java.util.List;


public class SelectAccountActivity extends Activity
{
    private List<Account> 				        _data;
	private ListView 					        _accountListView;
    private ParseQueryAdapter<ParseObject>      _adapter;
    private ServiceInfo                         _si;
    private NumberFormat                        _format = NumberFormat.getCurrencyInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
  		setContentView(R.layout.activity_select_account_layout);
		_accountListView = (ListView)findViewById(R.id.accountList);
        Bundle bundle = getIntent().getBundleExtra("serviceInfoBundle");
        _si = (ServiceInfo)bundle.getParcelable("serviceInfo");
        if (_si.getServiceID().intValue() == 0) {
            String text = "<font color=#FFCC00>Gas</font><font color=#FF9500>Natural</font>";
            setTitle(Html.fromHtml(text));
        } else if (_si.getServiceID().intValue() == 1) {
            String text = "<font color=#007AFF>I.D.A.A.N.</font>";
            setTitle(Html.fromHtml(text));
        } else {
            setTitle(_si.getDescription());
        }

		final ParseUser currentUser = ParseUser.getCurrentUser();
        final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
        final Resources res = getResources();
        pd.setMessage(res.getString(R.string.accountLoading));
        pd.show();
        ParseQueryAdapter.QueryFactory<ParseObject> factory =
                new ParseQueryAdapter.QueryFactory<ParseObject>() {
                    public ParseQuery create() {
                        ParseQuery query = new ParseQuery("account");
                        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
                        query.whereEqualTo("userID", currentUser.getObjectId());
                        query.whereEqualTo("ServiceID", _si.getServiceID());
                        query.whereEqualTo("enabled", true);
                        return query;
                    }
        };

        _adapter = new ParseQueryAdapter<ParseObject>(this, factory) {
            @Override
            public View getItemView(final ParseObject object, final View v, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater)SelectAccountActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.account_select_row_layout, parent, false);
                TextView textView1 = (TextView)rowView.findViewById(R.id.accountLbl);
                TextView textView2 = (TextView)rowView.findViewById(R.id.amountDueLbl);
                textView1.setText(object.getString("accountID"));
                Number amount_due = object.getNumber("amount_due");
                textView2.setText(_format.format(amount_due));
                Button btn = (Button)rowView.findViewById(R.id.paymentBtn);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startSelectPaymentType(v, object);
                    }
                });
                return rowView;
            }
        };

        _adapter.addOnQueryLoadListener(new OnQueryLoadListener<ParseObject>() {
            @Override
            public void onLoading() {
            }

            @Override
            public void onLoaded(List<ParseObject> parseObjects, Exception e) {
                pd.dismiss();
            }
        });

        _accountListView.setAdapter(_adapter);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.select_account_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.action_history:
                addNewAccount();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addNewAccount() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        input.setHint(R.string.selectAccountAddNewAccountHint);
        final Context context = getApplicationContext();
        builder.setTitle(String.format(context.getString(R.string.selectAccountAddNewAccountTitle), _si.getDescription()));
        builder.setView(input);
        builder.setMessage(R.string.selectAccountAddNewAccountBody);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                final Context ctx = getApplicationContext();
                final ProgressDialog pd = new ProgressDialog(SelectAccountActivity.this, ProgressDialog.STYLE_SPINNER);
                pd.setMessage(ctx.getString(R.string.selectAccountAddNewAccountValidation));
                ParseQuery query = new ParseQuery("account_repo");
                query.whereEqualTo("accountID", input.getText().toString().trim());
                query.whereEqualTo("serviceID", _si.getServiceID());
                query.getFirstInBackground(new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        pd.dismiss();
                        if (e != null) {
                            int duration = Toast.LENGTH_SHORT;
                            CharSequence text = ctx.getString(R.string.selectAccountAddNewAccountNotFound);
                            Toast toast = Toast.makeText(ctx, text, duration);
                            toast.show();
                        } else {
                            String objID = parseObject.getObjectId();
                            String userID = ParseUser.getCurrentUser().getObjectId();
                            String accountID = parseObject.getString("accountID");
                            Number amountDue = parseObject.getNumber("amount_due");
                            Date dueDate = parseObject.getDate("due_date");
                            Number serviceID = parseObject.getNumber("serviceID");
                            String customer = parseObject.getString("customer");
                            Account account = new Account(objID, userID, accountID, serviceID, amountDue, dueDate, customer, customer);
                            confirmAddAccount(account);
                        }
                    }
                });
            }
        });

        final AlertDialog dialog = builder.create();
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                Button btn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                btn.setEnabled(input.length() > 0);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
    }

    private void confirmAddAccount(final Account account) {
        final Context ctx = getApplicationContext();
        final ProgressDialog pd = new ProgressDialog(SelectAccountActivity.this, ProgressDialog.STYLE_SPINNER);

        String title = String.format(ctx.getString(R.string.selectAccountAddNewAccountConfirmTitle), account.getAccountID());
        String body = String.format(ctx.getString(R.string.selectAccountAddNewAccountConfirmBody), account.getCustomer());
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(body);

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                pd.setMessage(ctx.getString(R.string.selectAccountAddNewAccountAddition));
                pd.show();
                ParseObject newAccount = new ParseObject("account");
                newAccount.put("userID", account.getUserID());
                newAccount.put("ServiceID", account.getServiceID());
                newAccount.put("amount_due", account.getAmountDue());
                newAccount.put("due_date", account.getDueDate());
                newAccount.put("accountID", account.getAccountID());
                newAccount.put("customer", account.getCustomer());
                newAccount.put("enabled", true);
                newAccount.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        pd.dismiss();
                        if (e == null) {
                            _adapter.loadObjects();
                        }
                    }
                });
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void startSelectPaymentType(View v, ParseObject parseObject) {
        String objID = parseObject.getObjectId();
        String userID = ParseUser.getCurrentUser().getObjectId();
        String accountID = parseObject.getString("accountID");
        Number amountDue = parseObject.getNumber("amount_due");
        Date dueDate = parseObject.getDate("due_date");
        Number serviceID = parseObject.getNumber("ServiceID");
        String customer = parseObject.getString("customer");
        Account account = new Account(objID, userID, accountID, serviceID, amountDue, dueDate, customer, customer);
        Intent intent = new Intent(v.getContext(), SelectPaymentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("serviceInfo", _si);
        bundle.putParcelable("account", account);
        intent.putExtra("serviceInfoBundle", bundle);
        startActivityForResult(intent, 2);
    }
}
