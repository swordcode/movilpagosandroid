package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.swordcode.movilpagos.models.EventTicket;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by euclidesflores on 2/3/14.
 */
public class SelectMovieDatesActivity extends Activity {
	private String[]						_values;
	private ArrayAdapter<String>			_adapter;
	private ListView						_list;
	private EventTicket 					_event;
	private Date[]							_dates;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_movie_date_layout);
		_list = (ListView)findViewById(R.id.movie_dates_list);
		Bundle bundle = getIntent().getBundleExtra("eventInfoBundle");
		_event = (EventTicket)bundle.getParcelable("event");
		setTitle("Seleccione fecha");
		_dates = new Date[5];
		Date date = new Date();
		for (int i=0;i < 5; i++) {
			_dates[i] = date;
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DATE, 1);
			date = c.getTime();
		}
		_values = new String[5];
		SimpleDateFormat sdf;
		for (int i=0;i < 5; i++) {
			switch(i) {
				case 0: {
					sdf = new SimpleDateFormat("'Hoy', d 'de' MMMM", new Locale("es"));
					break;
				}
				case 1: {
					sdf = new SimpleDateFormat("'Mañana', d 'de' MMMM", new Locale("es"));
					break;
				}
				default: {
					sdf = new SimpleDateFormat("EEEE, d 'de' MMMM", new Locale("es"));
				}
			}
			String dateStr = sdf.format(_dates[i]);
			String output = dateStr.substring(0, 1).toUpperCase() + dateStr.substring(1);
			_values[i] = output;
		}
		_list.addHeaderView(createHeaderView(), null, false);
		_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,  _values);
		_list.setAdapter(_adapter);
		_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(SelectMovieDatesActivity.this.getApplicationContext(),
						SelectMovieDetailActivity.class);
				EventTicket event = new EventTicket(_event.getEventID(),
										_event.getEventName(), _event.getEventType(),
										_dates[i-1], _event.getEventSchedule(), _event.getEventRoom(),
										_event.getAmount(), 4.5f, _event.getSeats()

				);
				Bundle bundle = new Bundle();
				bundle.putParcelable("event", event);
				intent.putExtra("eventInfoBundle", bundle);
				startActivityForResult(intent, 6);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private View createHeaderView() {
		LayoutInflater inflater = (LayoutInflater)this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View headerView = inflater.inflate(R.layout.select_movie_date_header, null);
		return headerView;
	}
}
