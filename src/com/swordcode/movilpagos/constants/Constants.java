package com.swordcode.movilpagos.constants;


public interface Constants {
	public final static int ERR_InvalidObject 		= 101;
	public final static int ERR_UserNameMissing 	= 200;
	public final static int ERR_PasswordMissing 	= 201;
	public final static int ERR_UserNameTaken 		= 202;
	public final static int ERR_EmailTaken 			= 203;
	public final static int ERR_EmailMissing 		= 204;
	public final static int ERR_EmailNotFound 		= 205;
	public final static int ERR_Unknown 			= 900;

	public final static int ST_Utility				= 0;
	public final static int ST_Event				= 1;

    public final static int EVENT_EVENT 	= 0;
    public final static int EVENT_CONCERT 	= 1;
    public final static int EVENT_MOVIE 	= 2;

	public final static int ACTIVITY_CODE_TRX 	= 0x00008;
	public final static int ACTIVITY_CODE_DEF 	= 0x00010;
	public final static int ACTIVITY_CODE_SET 	= 0x00012;
}
