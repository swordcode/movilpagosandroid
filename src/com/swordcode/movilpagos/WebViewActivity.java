package com.swordcode.movilpagos;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/**
 * Created by euclidesflores on 3/14/14.
 */
public class WebViewActivity extends Activity {
	private WebView					_webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String url = getIntent().getStringExtra("url");
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		if (_webView == null) _webView = new WebView(this);
		setContentView(_webView);
		_webView.getSettings().setJavaScriptEnabled(true);
		final Activity activity = this;
		_webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				activity.setTitle(view.getTitle());
				activity.setProgress(progress * 1000);
			}
		});
		_webView.loadUrl(url);
	}
}
