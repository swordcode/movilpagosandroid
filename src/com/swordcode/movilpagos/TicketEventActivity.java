package com.swordcode.movilpagos;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.swordcode.movilpagos.adapters.PagerAdapter;

/**
 * Created by euclidesflores on 1/28/14.
 */
public class TicketEventActivity extends FragmentActivity implements
        ActionBar.TabListener
{

    private ViewPager               _viewPager;
    private PagerAdapter            _adapter;
    private ActionBar               _actionBar;
    private String[] tabs = { "Eventos", "Conciertos", "Cine" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_event_layout);
        _actionBar = getActionBar();
        _adapter = new PagerAdapter(getSupportFragmentManager());
        _viewPager = (ViewPager)findViewById(R.id.pager);
        _viewPager.setAdapter(_adapter);
        _actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (String tabName : tabs) {
            _actionBar.addTab(_actionBar.newTab().setText(tabName)
                    .setTabListener(this));
        }

        _viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                _actionBar.setSelectedNavigationItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        _viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
