package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.util.Contents;
import com.swordcode.movilpagos.util.QRCodeEncoder;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by euclidesflores on 2/7/14.
 */
public class EventCheckoutCompleteActivity extends Activity {
	private String						_trxID;
	private EventTicket					_event;
	private Menu 						_menu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_checkout_complete_layout);
		setTitle(R.string.eventOpenTicketHeader);
		_event = (EventTicket)getIntent().getParcelableExtra("event");
		_trxID = getIntent().getStringExtra("trx");
		TextView eventNameTxt = (TextView)findViewById(R.id.checkoutCompleteHeaderTxt);
		eventNameTxt.setText(_event.getEventName());
		TextView eventDateTxt = (TextView)findViewById(R.id.checkoutCompleteDateTxt);
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE, d 'de' MMMM yyyy", new Locale("es"));
		String dateStr = sdf.format(_event.getEventDate());
		String output = dateStr.substring(0, 1).toUpperCase() + dateStr.substring(1);
		output = output + " " + _event.getEventSchedule();
		eventDateTxt.setText(output);
		TextView seatTxt = (TextView)findViewById(R.id.checkoutCompleteSeatTxt);
		int seats = _event.getSeats().intValue();
		seatTxt.setText(String.format("%d %s", seats, seats > 1 ? "adultos" : "adulto"));
		TextView sectionTxt = (TextView)findViewById(R.id.checkoutCompleteSectionTxt);
		sectionTxt.setText(_event.getEventRoom());
		ImageView imageView = (ImageView)findViewById(R.id.qrImage);
		QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(_trxID, null,
				Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), 340);
		try {
			Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
			imageView.setImageBitmap(bitmap);
		} catch (WriterException e) {
			e.printStackTrace();
		}
		TextView bottomTxt = (TextView)findViewById(R.id.checkoutCompleteBottomTxt);
		Context ctx = getApplicationContext();
		String bottomStr = String.format("<b>%s</b>", ctx.getString(R.string.eventOpenBottomMessage2));
		bottomStr = String.format(ctx.getString(R.string.eventOpenBottomMessage2), _trxID);
		bottomStr = String.format("%s %s", ctx.getString(R.string.eventOpenBottomMessage1), bottomStr);
		bottomTxt.setText(Html.fromHtml(bottomStr));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case android.R.id.home: {
					finish();
					return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.checkout_complete_actions, menu);
		_menu = menu;
		return super.onCreateOptionsMenu(menu);
	}
}
