package com.swordcode.movilpagos;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.parse.ParseObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by euclidesflores on 2/5/14.
 */
public class MPExpandableListAdapter extends BaseExpandableListAdapter {
	private Context										_ctx;
	private List<String> 								_headers;
	private HashMap<String, List<ParseObject>>			_children;

	public MPExpandableListAdapter(Context ctx, List<String> headers, HashMap<String, List<ParseObject>> children) {
		_ctx = ctx;
		_headers = headers;
		_children = children;
	}

	@Override
	public int getGroupCount() {
		return _headers.size();
	}

	@Override
	public int getChildrenCount(int i) {
		return _children.get(_headers.get(i)).size();
	}

	@Override
	public Object getGroup(int i) {
		return _headers.get(i);
	}

	@Override
	public ParseObject getChild(int i, int i2) {
		return _children.get(_headers.get(i)).get(i2);
	}

	@Override
	public long getGroupId(int i) {
		return i;
	}

	@Override
	public long getChildId(int i, int i2) {
		return i2;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
		return null;
	}

	@Override
	public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
		return null;
	}

	@Override
	public boolean isChildSelectable(int i, int i2) {
		return true;
	}
}
