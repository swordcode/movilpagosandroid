package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.swordcode.movilpagos.utils.FormUtils;


public class LoginActivity extends Activity
{
	private EditText		_userName;
	private EditText		_password;
	private Button			_signIn;
	private Button			_forgotPassword;
	private Button			_signUp;
	private TextView		_mssg1;
	private TextView		_mssg2;
	private int				_selectedItem;
	ProgressDialog			_pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_layout);
		_mssg1 = (TextView)findViewById(R.id.mssg1);
		_userName = (EditText)findViewById(R.id.userName);
		_password = (EditText)findViewById(R.id.password);
		_signIn = (Button)findViewById(R.id.signIn);
		_signIn.setBackgroundColor(Color.parseColor("#007AFF"));
		_signIn.setTextColor(Color.WHITE);
		_forgotPassword = (Button)findViewById(R.id.forgotPassword);
		_forgotPassword.setTextColor(Color.parseColor("#007AFF"));
		_mssg1 = (TextView)findViewById(R.id.signUpText);
		_mssg1.setTextColor(Color.GRAY);
		_signUp = (Button)findViewById(R.id.signUp);
		_signUp.setBackgroundColor(Color.parseColor("#FF9500"));
		_signUp.setTextColor(Color.WHITE);
	}

	public void resetPassword(View view) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(this);
		final Context context = getApplicationContext();
		builder.setTitle(R.string.passwordResetTitle);
		builder.setMessage(R.string.passwordResetBody);
		input.setHint(R.string.passwordResetHint);
		builder.setView(input);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
		  public void onClick(DialogInterface dialog, int whichButton) {
		    // Canceled.
		  }
		});

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			  final String email = input.getText().toString();
			  final ProgressDialog pd = new ProgressDialog(LoginActivity.this, ProgressDialog.STYLE_SPINNER);
			  pd.setMessage(LoginActivity.this.getResources().getString(R.string.passwordResetCheckingEmail));
			  pd.show();
			  ParseUser.requestPasswordResetInBackground(email,  new RequestPasswordResetCallback() {
				  public void done(ParseException e) {
					  pd.dismiss();
					  int duration = Toast.LENGTH_LONG;
					  String mssg = context.getString(R.string.passwordResetSucceed);
					  mssg = String.format(mssg, email);
					  if (e != null) {
						  mssg = FormUtils.getErrorMssg(e.getCode(), context);
					  }
					  Toast toast = Toast.makeText(context, mssg, duration);
					  toast.show();
				  }
			  });
			}
		});

		final AlertDialog dialog = builder.create();
		input.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				Button btn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				btn.setEnabled(FormUtils.validateEmailField(input));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
	}
	
	public void signUp(View view) {
		Intent intent = new Intent(view.getContext(), SignUpActivity.class);
		startActivityForResult(intent, 0);
	}

	public void startSession(View view) {
		if (FormUtils.isEditFieldEmpty(_userName)) {
			AlertDialog dg = FormUtils.addLightDialog(R.string.loginMssgUserNameEmpty, this);
			dg.show();
			return;
		}
		if (FormUtils.isEditFieldEmpty(_password)) {
			AlertDialog dg = FormUtils.addLightDialog(R.string.loginMssgPasswordEmpty, this);
			dg.show();
			return;
		}

		final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
		final Resources res = getResources();
		pd.setMessage(res.getString(R.string.loginStartSessionMsg));
		pd.show();
		ParseUser.logInInBackground(FormUtils.getFieldText(_userName), FormUtils.getFieldText(_password), 
				new LogInCallback() {
					@Override
					public void done(ParseUser user, ParseException e) {
						pd.dismiss();
						Context context = getApplicationContext();
						int duration = Toast.LENGTH_SHORT;
						CharSequence text = res.getString(R.string.loginWrongNameOrPAsswordMssg);
						if (user == null) {
							//
						} else {
							text = res.getString(R.string.loginSessionStarted);
							Thread thread = new Thread() {
								@Override
								public void run() {
									try {
										Thread.sleep(Toast.LENGTH_SHORT);
										Intent returnIntent = new Intent();
										returnIntent.putExtra("selectedItem", getSelectedItem());
										setResult(RESULT_OK, returnIntent);     
										finish();
									} catch (Exception e) {
										e.printStackTrace();
									}
								}  
							};
							thread.start();
						}
						Toast toast = Toast.makeText(context, text, duration);
						toast.show();
					}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			String tmpUserName = data.getStringExtra("tmpUserName");
			_userName.setText(tmpUserName);
			_password.requestFocus();
		}
	}

	public int getSelectedItem() {
		Intent intent = getIntent();
		return intent.getIntExtra("selectedItem", 0);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
