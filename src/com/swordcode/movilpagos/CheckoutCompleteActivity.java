package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.swordcode.movilpagos.models.Transaction;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import static com.swordcode.movilpagos.constants.Constants.ACTIVITY_CODE_DEF;
import static com.swordcode.movilpagos.constants.Constants.ACTIVITY_CODE_TRX;

/**
 * Created by euclidesflores on 1/26/14.
 */
public class CheckoutCompleteActivity extends Activity
{
    private Transaction         _trx;
    private TextView            _paidTo;
    private TextView            _amount;
    private TextView            _date;
    private TextView            _card;
    private TextView            _ref;
    private Menu                _menu;
	private int					_requestCode;

    private NumberFormat        _format = NumberFormat.getCurrencyInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_checkout_complete_layout);
        setTitle(R.string.checkoutCompleteTitle);
        Bundle bundle = getIntent().getBundleExtra("trxBundle");
        _trx = (Transaction)bundle.getParcelable("trx");
		_requestCode = getIntent().getIntExtra("requestCode", ACTIVITY_CODE_DEF);
        _paidTo = (TextView)findViewById(R.id.checkoutCompletePaidToTxt);
        _amount = (TextView)findViewById(R.id.checkoutCompleteAmountTxt);
        _date = (TextView)findViewById(R.id.checkoutCompleteDateTxt);
        _card = (TextView)findViewById(R.id.checkoutCompleteCardTxt);
        _ref = (TextView)findViewById(R.id.checkoutCompleteRefTxt);
        _paidTo.setText(_trx.getServiceName());
        _amount.setText(_format.format(_trx.getAmount()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        _date.setText(formatter.format(_trx.getPaymentDate()));
        _card.setText(String.format("%s****%s", _trx.getCardType(), _trx.getLast4()));
        _ref.setText(_trx.getTrxID());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.checkout_complete_actions, menu);
        _menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
			case android.R.id.home: {
				if (_requestCode == ACTIVITY_CODE_TRX) {
					finish();
					return true;
				} else {
					return super.onOptionsItemSelected(item);
				}
			}
            case R.id.send_email: {
                 mailReceipt();
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void mailReceipt() {
        setProgressBarIndeterminateVisibility(true);
        Map<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("serviceName", _trx.getServiceName());
        dictionary.put("userID", _trx.getUserID());
        dictionary.put("amount", _format.format(_trx.getAmount()));
        dictionary.put("cardName", String.format("%s****%s", _trx.getCardType(), _trx.getLast4()));
        final Context ctx = getApplicationContext();
        ParseCloud.callFunctionInBackground("checkoutSendMail", dictionary, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                setProgressBarIndeterminateVisibility(false);
                if (e != null) {
                    Toast.makeText(ctx, e.getMessage(),
                            Toast.LENGTH_LONG
                    ).show();
                } else {
                    Toast.makeText(ctx, ctx.getText(R.string.checkoutCompleteMailSent),
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        });
    }

    public void closeReceiptWindow(View v) {
		if (_requestCode == ACTIVITY_CODE_TRX) {
			finish();
		} else {
        	NavUtils.navigateUpFromSameTask(this);
		}
    }
}
