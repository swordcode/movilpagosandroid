package com.swordcode.movilpagos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by euclidesflores on 1/29/14.
 */
@ParseClassName("Seat")
public class Seat extends ParseObject implements Parcelable
{
	private Number			_seatID;
	private Number 			_row;
	private Number			_price;
	private String			_eventID;
	private Number			_qty;
	private Number			_section;

	public Seat() {
	}

	public Seat(Number seatID, Number row, Number price, String eventID, Number qty, Number section) {
		_seatID = seatID;
		_row = row;
		_price = price;
		_eventID = eventID;
		_qty = qty;
	}

	public Seat(Number row, Number price, String eventID, Number qty, Number section) {
		_row = row;
		_price = price;
		_eventID = eventID;
		_qty = qty;
		_section = section;
	}

	public Number getSeatID() {
		return _seatID;
	}

	public Number getRow() {
		return _row;
	}

	public Number getPrice() {
		return _price;
	}

	public String getEventID() {
		return _eventID;
	}

	public Number getQty() {
		return _qty;
	}

	public Number getSection() {
		return _section;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(_row.intValue());
		parcel.writeDouble(_price.doubleValue());
		parcel.writeString(_eventID);
		parcel.writeInt(_qty.intValue());
		parcel.writeInt(_section.intValue());
	}

	public static final Parcelable.Creator<Seat> CREATOR = new Parcelable.Creator<Seat>() {
		public Seat createFromParcel(Parcel in) {
			return new Seat(in);
		}

		public Seat[] newArray(int size) {
			return new Seat[size];
		}
	};

	private Seat(Parcel in) {
		_row = in.readInt();
		_price = in.readDouble();
		_eventID = in.readString();
		_qty = in.readInt();
		_section = in.readInt();
	}
}
