package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseQuery;
import com.parse.ParseUser;

import static com.swordcode.movilpagos.constants.Constants.ACTIVITY_CODE_SET;

/**
 * Created by euclidesflores on 2/10/14.
 */
public class PreferencesActivity extends Activity
{
	private ListView								_listView;
	private ArrayAdapter<String> 					_adapter;
	private String[]								_array;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences_layout);
		setTitle(R.string.action_settings);
		_listView = (ListView)findViewById(R.id.preference_list);
		init();
		_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				selectItem(view, position);
			}
		});
	}

	private void selectItem(View view, int position) {
		final ParseUser currentUser = ParseUser.getCurrentUser();
		//
		// happen when user is not logged in
		//
		if (currentUser == null) {
			switch(position) {
				case 0: {
					// Login screen
					Intent intent = new Intent(view.getContext(), LoginActivity.class);
					startActivityForResult(intent, ACTIVITY_CODE_SET);
					break;
				}
				case 2: {
					// start email UI
					startEmail("soporte@movilpagos.net", "Soporte MovilPagos");
					break;
				}
				case 3: {
					makeCall();
					break;
				}
				case 5: {
					// start email UI
					startEmail("panama-feedback@movilpagos.net", "MovilPagos feedback");
					break;
				}
				case 7: {
					showWebView("https://www.movilpagos.net/index-5.html", view);
					break;
				}
				case 8: {
					showWebView("https://www.movilpagos.net/index-6	.html", view);
					break;
				}
			}
		} else {
			//
			// happen when user is logged in
			//
			switch(position) {
				case 0: {
					// Logout
					logout();
					break;
				}
				case 1: {
					//Edit profile
					Intent intent = new Intent(view.getContext(), UpdateProfileActivity.class);
					startActivityForResult(intent, ACTIVITY_CODE_SET);
					break;
				}
				case 2: {
					//Update password
					Intent intent = new Intent(view.getContext(), UpdatePasswordActivity.class);
					startActivityForResult(intent, ACTIVITY_CODE_SET);
					break;
				}
				case 4: {
					// start email UI
					startEmail("soporte@movilpagos.net", "Soporte MovilPagos");
					break;
				}
				case 5: {
					makeCall();
					break;
				}
				case 7: {
					// start email UI
					startEmail("panama-feedback@movilpagos.net", "MovilPagos feedback");
					break;
				}
				case 9: {
					showWebView("https://www.movilpagos.net/index-5.html", view);
					break;
				}
				case 10: {
					showWebView("https://www.movilpagos.net/index-6.html", view);
					break;
				}
			}
		}

	}

	private void showWebView(String url, View view) {
		Intent intent = new Intent(view.getContext(), WebViewActivity.class);
		intent.putExtra("url", url);
		startActivity(intent);
	}

	private void makeCall() {
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:62340550"));
		startActivity(intent);
	}

	private void startEmail(String to, String subject) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { to });
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		startActivity(intent);
	}

	private void logout() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final Context ctx = getApplicationContext();
		builder.setTitle(R.string.logoutMssg1);
		builder.setMessage(R.string.logoutMssg2);
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				ParseUser.logOut();
				ParseUser currentUser = ParseUser.getCurrentUser();
				ParseQuery.clearAllCachedResults();
				init();
			}
		});

		final AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void init() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		if (currentUser == null) {
			_array = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

		} else {
			_array = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
		}
		createAdapter();
		_listView.setAdapter(_adapter);
	}

	private void createAdapter() {
		final ParseUser currentUser = ParseUser.getCurrentUser();
		_adapter = new ArrayAdapter<String>(this, R.layout.event_row_layout, _array) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater)PreferencesActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView;
				//
				// happen when user is not logged in
				//
				if (currentUser == null) {
					switch (position) {
						case 0: {
							rowView = inflater.inflate(R.layout.preference_row1, parent, false);
							break;
						}
						case 1: {
							rowView = inflater.inflate(R.layout.preference_empty_row, parent, false);
							break;
						}
						case 2: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Email: soporte@movilpagos.net");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.mail);
							break;
						}
						case 3: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Llamar: 999-9999");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.iphone);
							break;
						}
						case 4: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Tweet: @movilpagos");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.twitter);
							break;
						}
						case 5: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Dejenos su opinión sobre este app");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.comment);
							break;
						}
						case 6: {
							rowView = inflater.inflate(R.layout.preference_empty_row, parent, false);
							break;
						}
						case 7: {
							rowView = inflater.inflate(R.layout.preference_row4, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Política de privacidad");
							break;
						}
						case 8: {
							rowView = inflater.inflate(R.layout.preference_row4, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Términos y condiciones de uso");
							break;
						}
						case 9: {
							rowView = inflater.inflate(R.layout.preference_row5, parent, false);
							break;
						}
						default: {
							rowView = inflater.inflate(R.layout.preference_row1, parent, false);
						}
					}
				//
				// happen when user is logged in
				//
				} else {
					switch (position) {
						case 0: {
							rowView = inflater.inflate(R.layout.preference_row2, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.col1);
							String text = String.format("¿No eres %s?", currentUser.getUsername());
							tf.setText(text);
							break;
						}
						case 1: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Editar mi perfil");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.avatar);
							break;
						}
						case 2: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Cambiar la contraseña");
							break;
						}
						case 3: {
							rowView = inflater.inflate(R.layout.preference_empty_row, parent, false);
							break;
						}
						case 4: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Email: soporte@movilpagos.net");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.mail);
							break;
						}
						case 5: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Llamar: 999-9999");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.iphone);
							break;
						}
						case 6: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Tweet: @movilpagos");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.twitter);
							break;
						}
						case 7: {
							rowView = inflater.inflate(R.layout.preference_row3, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Dejenos su opinión sobre este app");
							ImageView imageView = (ImageView)rowView.findViewById(R.id.icon);
							imageView.setImageResource(R.drawable.comment);
							break;
						}
						case 8: {
							rowView = inflater.inflate(R.layout.preference_empty_row, parent, false);
							break;
						}
						case 9: {
							rowView = inflater.inflate(R.layout.preference_row4, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Política de privacidad");
							break;
						}
						case 10: {
							rowView = inflater.inflate(R.layout.preference_row4, parent, false);
							TextView tf = (TextView)rowView.findViewById(R.id.description);
							tf.setText("Términos y condiciones de uso");
							break;
						}
						case 11: {
							rowView = inflater.inflate(R.layout.preference_row5, parent, false);
							break;
						}
						default: {
							rowView = inflater.inflate(R.layout.preference_row1, parent, false);
						}
					}
				}
				return rowView;
			}
		};
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		final ParseUser currentUser = ParseUser.getCurrentUser();
		if (requestCode == ACTIVITY_CODE_SET) {
			if (currentUser != null) {
				init();
			}
		}
	}
}
