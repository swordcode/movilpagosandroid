package com.swordcode.movilpagos.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.swordcode.movilpagos.R;

import static com.swordcode.movilpagos.constants.Constants.ERR_EmailMissing;
import static com.swordcode.movilpagos.constants.Constants.ERR_EmailNotFound;
import static com.swordcode.movilpagos.constants.Constants.ERR_EmailTaken;
import static com.swordcode.movilpagos.constants.Constants.ERR_InvalidObject;
import static com.swordcode.movilpagos.constants.Constants.ERR_PasswordMissing;
import static com.swordcode.movilpagos.constants.Constants.ERR_UserNameMissing;
import static com.swordcode.movilpagos.constants.Constants.ERR_UserNameTaken;

public class FormUtils 
{
	public static boolean isEditFieldEmpty(EditText et) {
		if (et == null) return true;
			String text = et.getText().toString();
			if (TextUtils.isEmpty(text)) return true;
		return false;
	}

	public static AlertDialog addLightDialog(int textID, Context ctx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setPositiveButton(android.R.string.ok, null);
		builder.setMessage(textID);
		return builder.create();
	}

	public static String getFieldText(TextView tv) {
		if (tv == null)  return "";
		return tv.getText().toString().trim();
	}

	public static boolean validateEmailField(EditText et) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(getFieldText(et)).matches();
	}

	public static String getErrorMssg(int v, Context ctx) {
		switch (v) {
			case (ERR_InvalidObject) : {
				return ctx.getString(R.string.errorInvalidObject);
			}
			case (ERR_UserNameMissing) : {
				return ctx.getString(R.string.errorUserNameMissing);
			}
			case (ERR_PasswordMissing) : {
				return ctx.getString(R.string.errorPasswordMissing);
			}
			case (ERR_UserNameTaken) : {
				return ctx.getString(R.string.errorUserNameTaken);
			}
			case (ERR_EmailTaken) : {
				return ctx.getString(R.string.errorEmailTaken);
			}
			case (ERR_EmailMissing) : {
				return ctx.getString(R.string.errorEmailMissing);
			}
			case (ERR_EmailNotFound) : {
				return ctx.getString(R.string.errorEmailNotFound);
			}
			default: {
				return ctx.getString(R.string.unknownError);
			}
		}
	}
}
