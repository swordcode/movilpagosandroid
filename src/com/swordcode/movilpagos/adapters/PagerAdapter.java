package com.swordcode.movilpagos.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tabs.EventTab;
import static com.swordcode.movilpagos.constants.Constants.*;

/**
 * Created by euclidesflores on 1/28/14.
 */
public class PagerAdapter extends FragmentPagerAdapter
{

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
               return new EventTab(EVENT_EVENT);
            case 1:
                return new EventTab(EVENT_CONCERT);
            case 2:
                return new EventTab(EVENT_MOVIE);
        }
        return null;
    }


    @Override
    public int getCount() {
        return 3;
    }
}
