package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.models.ServiceInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.swordcode.movilpagos.constants.Constants.ACTIVITY_CODE_TRX;

/**
 * Created by euclidesflores on 2/4/14.
 */
public class PaymentHistoryActivity extends Activity {
//	private ParseQueryAdapter<ParseObject> 		_adapter;
//	private ListView 							_accountListView;

	private TextView 									_emptyMssg;
	private ExpandableListView							_listView;
	private MPExpandableListAdapter						_listAdapter;
	private List<String> 								_headerList = new ArrayList<String>();
	private HashMap<String, List<ParseObject>> 			_children 	= new HashMap<String, List<ParseObject>>();
	private HashMap<Integer, ServiceInfo>				_services 	= new HashMap<Integer, ServiceInfo>();
	private SimpleDateFormat							_sdf 		= new SimpleDateFormat("EEEE, d 'de' MMMM yyyy h:m a", new Locale("es"));


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setTitle(R.string.historyTitle);
		setContentView(R.layout.activity_history_layout);
		_listView = (ExpandableListView)findViewById(R.id.expandable_account_list);
		final ParseUser currentUser = ParseUser.getCurrentUser();
		_emptyMssg = (TextView)findViewById(android.R.id.empty);
		Bundle bundle = getIntent().getBundleExtra("serviceInfoBundle");
		ArrayList<ServiceInfo> services = bundle.getParcelableArrayList("serviceInfoList");
		for (int i=0;i<services.size(); i++) {
			ServiceInfo si = services.get(i);
			_services.put(si.getServiceID().intValue(), si);
		}
		if (currentUser != null) {
			_emptyMssg.setText("Cargando historial de pagos");
			setProgressBarIndeterminateVisibility(true);
			ParseQuery<ParseObject> query = ParseQuery.getQuery("account");
			query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
			query.whereEqualTo("userID", currentUser.getObjectId());
			query.whereEqualTo("enabled", true);
			query.orderByAscending("ServiceID");
			query.findInBackground(new FindCallback<ParseObject>() {
				@Override
				public void done(List<ParseObject> parseObjects, ParseException e) {
					_children 	= new HashMap<String, List<ParseObject>>();
					if (parseObjects != null) {
						_emptyMssg.setVisibility(TextView.GONE);
						for (ParseObject o : parseObjects) {
							Number serviceID = o.getNumber("ServiceID");
							String serviceName = _services.get(serviceID.intValue()).getServiceName();
							if (serviceName.equals("Comprar boletos")) {
								serviceName = "BOLETOS COMPRADOS";
							}
							if (!_headerList.contains(serviceName)) _headerList.add(serviceName);
							List<ParseObject> tmpList = _children.get(serviceName);
							if (tmpList == null) tmpList = new ArrayList<ParseObject>();
							tmpList.add(o);
							_children.put(serviceName, tmpList);
						}
						_listAdapter = new MPExpandableListAdapter(PaymentHistoryActivity.this, _headerList, _children) {
							@Override
							public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
								LayoutInflater inflater = (LayoutInflater)PaymentHistoryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								View rowView = inflater.inflate(R.layout.account_header_row_layout, viewGroup, false);
								String headerTitle = (String)getGroup(i);
								TextView textView1 = (TextView)rowView.findViewById(R.id.accountHeaderTxt);
								textView1.setText(headerTitle);
								return rowView;
							}

							@Override
							public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
								LayoutInflater inflater = (LayoutInflater)PaymentHistoryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								View rowView = inflater.inflate(R.layout.account_item_row_layout, viewGroup, false);
								TextView textView1 = (TextView)rowView.findViewById(R.id.accountItemTxt);
								TextView textView2 = (TextView)rowView.findViewById(R.id.accountItemDetailTxt);
								final ParseObject o = getChild(i, i2);
								textView1.setText(o.getString("accountID"));
								textView2.setText(o.getString("customer"));
								int serviceID = o.getInt("ServiceID");
								if (serviceID == 2) {
									int accountID = Integer.valueOf(o.getString("accountID"));
									Log.d("AccountID", String.valueOf(accountID));
									String eventName;
									switch (accountID) {
										case 1: {
											eventName = "The Hobbit: The desolation of Smaug";
											break;
										}
										case 2: {
											eventName = "Lone Survivor";
											break;
										}
										case 3: {
											eventName = "The Secret Life of Walter Mitty";
											break;
										}
										case 4: {
											eventName = "Cirque du Soleil";
											break;
										}
										case 5: {
											eventName = "Maroon 5 en Concierto";
											break;
										}
										default:
											eventName = "";
									}
									textView1.setText(eventName);
									String dateOfPurchase = _sdf.format(o.getCreatedAt());
									String output = dateOfPurchase.substring(0, 1).toUpperCase() + dateOfPurchase.substring(1);
									textView2.setText(output);
								}
								return rowView;
							}
						};
						_listView.setAdapter(_listAdapter);
					} else {
						_emptyMssg.setText(R.string.emptyListStr);
					}
					setProgressBarIndeterminateVisibility(false);
				}
			});
		}
		_listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
				ServiceInfo	si = _services.get(i);
				String serviceName = si.getServiceName().equals("Comprar boletos") ? "BOLETOS COMPRADOS" : si.getServiceName();
				ParseObject o = _children.get(serviceName).get(i2);
				Date dueDate = o.getDate("due_date") == null ? o.getCreatedAt() : o.getDate("due_date");
				Account account = new Account(
					o.getObjectId(), o.getString("userID"), o.getString("accountID"),
					o.getNumber("ServiceID"), o.getNumber("amount_due"), dueDate,
					o.getString("customer"), ""
				);

				Bundle bundle = new Bundle();
				bundle.putParcelable("serviceInfo", si);
				bundle.putParcelable("account", account);
				switch (si.getServiceID().intValue()) {
					case 2: {
						final Context ctx = getApplicationContext();
						final Intent intent = new Intent(getApplicationContext(), EventCheckoutCompleteActivity.class);
						final ProgressDialog pd = new ProgressDialog(PaymentHistoryActivity.this, ProgressDialog.STYLE_SPINNER);
						pd.setMessage(ctx.getString(R.string.eventOpenTicketMessage));
						pd.show();
						ParseQuery<ParseObject> query = ParseQuery.getQuery("EventTicket");
						query.whereEqualTo("accObjID", account.getObjID());
						query.getFirstInBackground(new GetCallback<ParseObject>() {
							@Override
							public void done(ParseObject o, ParseException e) {
								if (e == null) {
									EventTicket event = new EventTicket(
										o.getString("eventID"), o.getString("eventName"),
										o.getNumber("eventType"), o.getDate("eventDate"),
										o.getString("eventSchedule"), o.getString("eventRoom"),
										o.getNumber("amount"), o.getNumber("amountEach"),
										o.getNumber("seats")
									);

									intent.putExtra("trx", o.getString("transactionID"));
									intent.putExtra("event", event);
									startActivityForResult(intent, ACTIVITY_CODE_TRX);
								} else {
									Toast.makeText(getApplicationContext(),
											"Un error ocurrio abriendo el boleto.  Intente nuevamente.",
											Toast.LENGTH_LONG).show();
								}
								pd.dismiss();
							}
						});
						break;
					}
					default: {
						final Intent intent = new Intent(getApplicationContext(), PaymentHistoryDetailActivity.class);
						intent.putExtra("serviceAndAccountBundle", bundle);
						startActivityForResult(intent, 9);
					}
				}
				return false;
			}
		});
//		_accountListView = (ListView)findViewById(R.id.accountList);
//		final ParseUser currentUser = ParseUser.getCurrentUser();
//		if (currentUser != null) {
//			setProgressBarIndeterminateVisibility(true);
//			ParseQueryAdapter.QueryFactory<ParseObject> factory =
//					new ParseQueryAdapter.QueryFactory<ParseObject>() {
//						public ParseQuery create() {
//							ParseQuery query = new ParseQuery("account");
//							query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
//							query.whereEqualTo("userID", currentUser.getObjectId());
//							query.whereEqualTo("enabled", true);
//							return query;
//						}
//					};
//			_adapter = new ParseQueryAdapter<ParseObject>(this, factory) {
//				@Override
//				public View getItemView(final ParseObject object, final View v, ViewGroup parent) {
//					LayoutInflater inflater = (LayoutInflater)PaymentHistoryActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//					View rowView = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
//					TextView textView1 = (TextView)rowView.findViewById(android.R.id.text1);
//					TextView textView2 = (TextView)rowView.findViewById(android.R.id.text2);
//					textView1.setText(object.getString("accountID"));
//					textView2.setText(object.getString("customer"));
//					return rowView;
//				}
//			};
//
//			_adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
//				@Override
//				public void onLoading() {
//					_emptyMssg.setText("Cargando información de cuentas");
//				}
//
//				@Override
//				public void onLoaded(List<ParseObject> parseObjects, Exception e) {
//					if (parseObjects != null && parseObjects .size() < 1) {
//						_emptyMssg.setText(R.string.emptyListStr);
//					} else {
//						_emptyMssg.setVisibility(TextView.GONE);
//					}
//					setProgressBarIndeterminateVisibility(false);
//				}
//			});
//
//			_accountListView.setAdapter(_adapter);
//		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
