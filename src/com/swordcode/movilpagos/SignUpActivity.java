package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.swordcode.movilpagos.utils.FormUtils;

public class SignUpActivity extends Activity
{
	private TextView		_mssg1;
	private TextView		_mssg2;
	private EditText		_userName;
	private EditText		_fullName;
	private EditText 		_email;
	private EditText 		_password;
	private Button			_signUp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_layout);
		_mssg1 = (TextView)findViewById(R.id.signUpMssg1);

		_userName = (EditText)findViewById(R.id.signUpUserName);
		_fullName = (EditText)findViewById(R.id.signUpfullName);
		_email = (EditText)findViewById(R.id.signUpEmail);
		_password = (EditText)findViewById(R.id.password);
		_signUp = (Button)findViewById(R.id.signUp);
		_signUp.setBackgroundColor(Color.parseColor("#007AFF"));
		_signUp.setTextColor(Color.WHITE);
	}

	public void signUp(final View view) {
		EditText[] array = {_userName, _fullName, _email, _password};
		boolean isFormValid = true;
		int messageID = R.string.unknownError;
		for (int i=0;i < array.length; i++) {
			if (FormUtils.isEditFieldEmpty(array[i])) {
				switch(i) {
					case 0: {
						messageID = R.string.loginMssgUserNameEmpty;
						break;
					}
					case 1: {
						messageID = R.string.loginMssgFullNameEmpty;
						break;
					}
					case 2: {
						messageID = R.string.loginMssgEmailEmpty;
						break;
					}
					default: {
						messageID = R.string.loginMssgPasswordEmpty;
					}
				}
				isFormValid = false;
				break;
			}
		}

		if (isFormValid && FormUtils.getFieldText(_password).length() < 7) {
			messageID = R.string.loginMssgShortPassword;
			isFormValid = false;
		}

		if (isFormValid && !FormUtils.validateEmailField(_email)) {
			messageID = R.string.loginMssgEmailEmpty;
			isFormValid = false;
		}

		if (!isFormValid) {
			AlertDialog dg = FormUtils.addLightDialog(messageID, this);
			dg.show();
		} else {
			ParseUser user = new ParseUser();
			final String userName = FormUtils.getFieldText(_userName);
			user.setUsername(userName);
			user.setEmail(FormUtils.getFieldText(_email));
			user.setPassword(FormUtils.getFieldText(_password));
			user.put("additional", FormUtils.getFieldText(_userName));
			final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
			final Resources res = getResources();
			pd.setMessage(res.getString(R.string.signUpCreatingUser));
			pd.show();
			user.signUpInBackground(new SignUpCallback() {
				@Override
				public void done(ParseException e) {
					pd.dismiss();
					Context context = getApplicationContext();
					int duration = Toast.LENGTH_LONG;
					CharSequence text = res.getString(R.string.signUpUserCreated);
					String mssg = String.format(text.toString(), userName);
					if (e != null) {
						mssg = FormUtils.getErrorMssg(e.getCode(), view.getContext());
					} else {
						Thread thread = new Thread() {
							@Override
							public void run() {
								try {
									Thread.sleep(Toast.LENGTH_SHORT);
									Intent returnIntent = new Intent();
									returnIntent.putExtra("tmpUserName", userName);
									setResult(RESULT_OK,returnIntent);     
									finish();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}  
						};
						thread.start();
					}
					Toast toast = Toast.makeText(context, mssg, duration);
					toast.show();
				}
			});
		}
	}
}
