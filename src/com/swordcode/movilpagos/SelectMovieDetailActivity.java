package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.swordcode.movilpagos.models.EventTicket;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by euclidesflores on 2/3/14.
 */
public class SelectMovieDetailActivity extends Activity
{
	private EventTicket				_event;
	private ArrayAdapter<String>	_adapter;
	private ListView				_list;
	private String[]				_values;
	private int						_qty		= 1;
	private double					_amount;
	private SeekBar					 _seekBar;
	private NumberFormat _format	= NumberFormat.getCurrencyInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_movie_detail);
		Bundle bundle = getIntent().getBundleExtra("eventInfoBundle");
		_event = (EventTicket)bundle.getParcelable("event");
		SimpleDateFormat sdf = new SimpleDateFormat("d-MMM-y", new Locale("es"));
		_values = new String[]{"1", "2", "3", "4", "5"};
		_amount = _event.getAmount().doubleValue();
		setTitle(sdf.format(_event.getEventDate()) + "/" + _event.getEventName());
		_list = (ListView)findViewById(R.id.movie_detail_list);
		_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,  _values) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater)SelectMovieDetailActivity.this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView;
				switch (position) {
					case 1: {
						rowView = inflater.inflate(R.layout.movie_detail_row2, parent, false);
						break;
					}
					case 2: {
						rowView = inflater.inflate(R.layout.movie_detail_row3, parent, false);
						_seekBar = (SeekBar)rowView.findViewById(R.id.slider2);
						_seekBar.setMax(9);
						final TextView qtyTxt = (TextView)rowView.findViewById(R.id.movieDetailQtyTxt);
						qtyTxt.setText("1");
						_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
							int progressChanged = 1;
							@Override
							public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
								String value = String.valueOf(i+1);
								qtyTxt.setText(value);
								progressChanged = i+1;
								_qty = progressChanged;
								updateAmountFld();
							}

							@Override
							public void onStartTrackingTouch(SeekBar seekBar) {

							}

							@Override
							public void onStopTrackingTouch(SeekBar seekBar) {
							}
						});
						break;
					}
					case 3: {
						rowView = inflater.inflate(R.layout.movie_detail_row4, parent, false);
						TextView amountEach = (TextView)rowView.findViewById(R.id.movieDetailAmountTxt);
						amountEach.setText(_format.format(_event.getAmountEach()));
						break;
					}
					case 4: {
						rowView = inflater.inflate(R.layout.movie_detail_row4, parent, false);
						TextView totalTxt = (TextView)rowView.findViewById(R.id.movieDetailAmountTxt);
						totalTxt.setTypeface(null, Typeface.BOLD);
						totalTxt.setText(_format.format(_event.getAmountEach()));
						TextView totalLbl = (TextView)rowView.findViewById(R.id.movieDetailAmountLbl);
						totalLbl.setText("Total");
						totalLbl.setTypeface(null, Typeface.BOLD);
						break;
					}
					case 5: {
						rowView = inflater.inflate(R.layout.event_precheckout_row3, parent, false);
						break;
					}
					default: {
						rowView = inflater.inflate(R.layout.movie_detail_row1, parent, false);
					}
				}
				return  rowView;
			}
		};
		_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

			}
		});
		_list.setAdapter(_adapter);
	}

	private void updateAmountFld() {
		View view = _list.getChildAt(4);
		if (view != null) {
			TextView amount = (TextView)view.findViewById(R.id.movieDetailAmountTxt);
			double oldAmount = _event.getAmountEach().doubleValue();
			double newAmount = oldAmount * _qty;
			amount.setText(_format.format(newAmount));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}

}
