package com.swordcode.movilpagos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

/**
 * Created by euclidesflores on 1/25/14.
 */

@ParseClassName("transactions")
public class Transaction extends ParseObject implements Parcelable
{
    private String              _trxID;
    private String              _userID;
    private String              _accountID;
    private Date                _paymentDate;
    private Number              _amount;
    private Number              _balance;
    private Number              _serviceID;
    private String              _stripePaymentID;
    private String              _last4;
    private String              _cardType;
    private Number              _status;
    private String              _serviceName;
    private String              _formattedDate;

	public Transaction() {
	}

    public Transaction(String trxID) {
		_trxID = trxID;
    }

    public Transaction(String trxID, String userID, String accountID, Date paymentDate,
                       Number amount, Number balance, Number serviceID, String stripePaymentID,
                       String last4, String cardType, Number status, String serviceName) {
        _trxID = trxID;
        _userID = userID;
        _accountID = accountID;
        _paymentDate = paymentDate;
        _amount = amount;
        _balance = balance;
        _serviceID = serviceID;
        _stripePaymentID = stripePaymentID;
        _last4 = last4;
        _cardType = cardType;
        _status = status;
        _serviceName = serviceName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_trxID);
        parcel.writeString(_userID);
        parcel.writeString(_accountID);
        parcel.writeLong(_paymentDate.getTime());
        parcel.writeDouble(_amount.doubleValue());
        parcel.writeDouble(_balance.doubleValue());
        parcel.writeInt(_serviceID.intValue());
        parcel.writeString(_stripePaymentID);
        parcel.writeString(_last4);
        parcel.writeString(_cardType);
        parcel.writeInt(_status.intValue());
        parcel.writeString(_serviceName);
    }

    private Transaction(Parcel in) {
        _trxID = in.readString();
        _userID = in.readString();
        _accountID = in.readString();
        _paymentDate = new Date(in.readLong());
        _amount = in.readDouble();
        _balance = in.readDouble();
        _serviceID = in.readInt();
        _stripePaymentID = in.readString();
        _last4 = in.readString();
        _cardType = in.readString();
        _status = in.readInt();
        _serviceName = in.readString();
    }

    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public String getTrxID() {
        return _trxID;
    }

    public String getUserID() {
        return _userID;
    }

    public String getAccountID() {
        return _accountID;
    }

    public Date getPaymentDate() {
        return _paymentDate;
    }

    public Number getAmount() {
        return _amount;
    }

    public Number getBalance() {
        return _balance;
    }

    public Number getServiceID() {
        return _serviceID;
    }

    public String getStripePaymentID() {
        return _stripePaymentID;
    }

    public String getLast4() {
        return _last4;
    }

    public String getCardType() {
        return _cardType;
    }

    public Number getStatus() {
        return _status;
    }

    public String getServiceName() {
        return _serviceName;
    }

    public String getFormattedDate() {
        return _formattedDate;
    }
}
