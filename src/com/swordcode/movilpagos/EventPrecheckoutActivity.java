package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.models.Seat;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by euclidesflores on 2/1/14.
 */
public class EventPrecheckoutActivity extends Activity
{
	private EventTicket					_event;
	private Seat						_seat;
	private ListView					_list;
	private ArrayAdapter<String>		_adapter;
	private SeekBar						_seekBar;
	private NumberFormat 				_format = NumberFormat.getCurrencyInstance();
	private int							_progressChanged;
	private double						_amount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_precheckout_layout);
		_list = (ListView)findViewById(R.id.event_precheckout_list);
		Bundle bundle = getIntent().getBundleExtra("eventAndSeatBundle");
		_event = (EventTicket)bundle.getParcelable("event");
		_seat = (Seat)bundle.getParcelable("seat");
		_progressChanged = 1;
		_amount = _event.getAmountEach().doubleValue();
		setTitle(_event.getEventName());
		String[] strArr = {"1", "2", "3", "4", "5", "6", "7"};
		_adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.event_precheckout_row1, strArr) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater)EventPrecheckoutActivity.this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView;
				switch (position) {
					case 1: {
						rowView = inflater.inflate(R.layout.event_precheckout_row2, parent, false);
						TextView c1 = (TextView)rowView.findViewById(R.id.col1);
						c1.setText("Sección");
						TextView c2 = (TextView)rowView.findViewById(R.id.col2);
						c2.setText(_seat.getSection().toString());
						break;
					}
					case 2: {
						rowView = inflater.inflate(R.layout.event_precheckout_row2, parent, false);
						TextView c1 = (TextView)rowView.findViewById(R.id.col1);
						c1.setText("Fila");
						TextView c2 = (TextView)rowView.findViewById(R.id.col2);
						c2.setText(_seat.getRow().toString());
						break;
					}
					case 4: {
						rowView = inflater.inflate(R.layout.event_precheckout_row2, parent, false);
						TextView c1 = (TextView)rowView.findViewById(R.id.col1);
						c1.setText("Precio (c/u)");
						TextView c2 = (TextView)rowView.findViewById(R.id.col2);
						c2.setText(_format.format(_seat.getPrice()));
						break;
					}
					case 3: {
						rowView = inflater.inflate(R.layout.event_precheckout_row4, parent, false);
						_seekBar = (SeekBar)rowView.findViewById(R.id.slider1);
						_seekBar.setMax(_seat.getQty().intValue() - 1);
						final TextView qty = (TextView)rowView.findViewById(R.id.col2);
						qty.setText("1");
						_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
							int progressChanged = 1;
							@Override
							public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
								String value = String.valueOf(i+1);
								qty.setText(value);
								progressChanged = i+1;
								_progressChanged = progressChanged;
								updateAmountFld();
							}

							@Override
							public void onStartTrackingTouch(SeekBar seekBar) {

							}

							@Override
							public void onStopTrackingTouch(SeekBar seekBar) {
							}
						});
						break;
					}
					case 5: {
						rowView = inflater.inflate(R.layout.event_precheckout_row2, parent, false);
						TextView c1 = (TextView)rowView.findViewById(R.id.col1);
						c1.setTypeface(null, Typeface.BOLD);
						c1.setText("Total");
						final TextView amount = (TextView)rowView.findViewById(R.id.col2);
						amount.setTypeface(null, Typeface.BOLD);
						amount.setText(_format.format(_seat.getPrice()));
						break;
					}
					case 6: {
						rowView = inflater.inflate(R.layout.event_precheckout_row3, parent, false);
						break;
					}
					default: {
						rowView = inflater.inflate(R.layout.event_precheckout_row1, parent, false);
						TextView eventName = (TextView)rowView.findViewById(R.id.eventPreCheckoutEventName);
						eventName.setText(_event.getEventName());
						SimpleDateFormat sdf = new SimpleDateFormat("EEEE, d 'de' MMM y h:m a", new Locale("es", "ES"));
						TextView evenDate = (TextView)rowView.findViewById(R.id.eventPreCheckoutEventDate);
						String dateStr = sdf.format(_event.getEventDate());
						String output = dateStr.substring(0, 1).toUpperCase() + dateStr.substring(1);
						evenDate.setText(output);
					}
				}
				return rowView;
			}
		};
		final DataSetObserver observer = new DataSetObserver() {
			@Override
			public void onChanged() {
				updateAmountFld();
			}
		};

		_adapter.registerDataSetObserver(observer);
		_list.setAdapter(_adapter);
	}

	public final void updateAmountFld() {
		View view = _list.getChildAt(5);
		if (view != null) {
			TextView amount = (TextView)view.findViewById(R.id.col2);
			double oldAmount = _seat.getPrice().doubleValue();
			double newAmount = oldAmount * _progressChanged;
			amount.setText(_format.format(newAmount));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
