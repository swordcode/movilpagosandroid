package com.swordcode.movilpagos.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.swordcode.movilpagos.models.EventTicket;
/**
 * Created by euclidesflores on 1/29/14.
 */
public class EventArrayAdapter extends ArrayAdapter<String> {

	private EventTicket[]		_events;
	private String[]			_values;
	private final Context 		_context;

	public EventArrayAdapter(Context context, int resource, String[] values) {
		super(context, resource, values);
		_context = context;
		_values = values;
	}

	public EventArrayAdapter(Context context, int resource, String[] values, EventTicket[] events) {
		super(context, resource, values);
		_context = context;
		_values = values;
		_events = events;
	}

//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		View rowView = inflater.inflate(R.layout.event_row_layout, parent, false);
//		TextView textView = (TextView)rowView.findViewById(R.id.eventText);
//		textView.setText(_values[position]);
//		Button btn = (Button)rowView.findViewById(R.id.eventBuyBtn);
//		ImageView imageView = (ImageView)rowView.findViewById(R.id.eventImage);
//		final EventTicket event = _events[position];
//		btn.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				selectEvent(view, event);
//			}
//		});
//		int eventID = Integer.parseInt(event.getEventID());
// 	 	switch (eventID) {
//			case 1:
//				imageView.setImageResource(R.drawable.thehobbit);
//				break;
//			case 2:
//				imageView.setImageResource(R.drawable.lonesurvivorposter731f);
//				break;
//			case 3:
//				imageView.setImageResource(R.drawable.waltermitty);
//				break;
//			case 4:
//				imageView.setImageResource(R.drawable.circodelsol);
//				break;
//			case 5:
//				imageView.setImageResource(R.drawable.maroon_5);
//				break;
//		}
//		return rowView;
//	}

//	public void selectEvent(View v, EventTicket event) {
//		switch (event.getEventType().intValue()) {
//			case EVENT_MOVIE:
//				break;
//			default:
//				Intent intent = new Intent(v.getContext(), SelectEventSeatActivity.class);
//				Bundle bundle = new Bundle();
//				bundle.putParcelable("event", event);
//				intent.putExtra("eventInfoBundle", bundle);
//				_context.startActivity(intent, bundle);
////				v.getContext().startActivity(intent, bundle);
//		}
//	}
}
