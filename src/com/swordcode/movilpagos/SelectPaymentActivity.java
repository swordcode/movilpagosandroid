package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import com.stripe.android.model.Card;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.ServiceInfo;
import com.swordcode.movilpagos.utils.FormUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by euclidesflores on 1/21/14.
 */
public class SelectPaymentActivity  extends Activity implements PopupMenu.OnMenuItemClickListener {
    private EditText            _ccName;
    private EditText            _ccNumber;
    private EditText            _ccSecCode;

    private Button               _submitBtn;
    private Button               _testCreditCardBtn;
    private Button               _monthBtn;
    private Button               _yearBtn;
    private int                  _monthID = 1;
    private int                  _yearID = 2015;
    private ServiceInfo          _si;
    private Account              _account;
    private Card                 _card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getApplicationContext().getString(R.string.paymentFormTitle));
        setContentView(R.layout.activity_payment_form);
        Bundle bundle = getIntent().getBundleExtra("serviceInfoBundle");
        _si = (ServiceInfo)bundle.getParcelable("serviceInfo");
        _account = (Account)bundle.getParcelable("account");
        _ccName = (EditText)findViewById(R.id.ccName);
        _ccNumber = (EditText)findViewById(R.id.ccNumber);
        _ccSecCode = (EditText)findViewById(R.id.ccCode);
        _submitBtn = (Button)findViewById(R.id.selectPaymentCommitBtn);
        _testCreditCardBtn = (Button)findViewById(R.id.paymentFormTestCard);
        _monthBtn = (Button)findViewById(R.id.monthBtn);
        _yearBtn = (Button)findViewById(R.id.yearBtn);
        _monthBtn.setText(R.string.jan);
        _yearBtn.setText(R.string.y2015);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void validateCreditCardInfo(View v) {
        EditText[] array = {_ccName, _ccNumber, _ccSecCode};
        for (int i=0; i < array.length; i++) {
            if (FormUtils.isEditFieldEmpty(array[i])) {
                int mssgID;
                switch (i) {
                    case 1: {
                        mssgID = R.string.ccEmptyName;
                        break;
                    }
                    case 2:
                        mssgID = R.string.ccEmptySecCode;
                        break;
                    default:
                        mssgID = R.string.ccEmptyNumber;
                }
                AlertDialog ad = FormUtils.addLightDialog(R.string.ccEmptyTitle, this);
                ad.setTitle(mssgID);
                ad.show();
                return;
            }
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateStr = String.format("01-%d-%d", _monthID, _yearID);
        Date date = new Date();
        try {
            date = formatter.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date today = new Date();
        if (date.compareTo(today) < 0) {
            AlertDialog ad = FormUtils.addLightDialog(R.string.ccInvalidDate, this);
            ad.show();
            return;
        }
        String cardNumber = FormUtils.getFieldText(_ccNumber);
        String cardCode = FormUtils.getFieldText(_ccSecCode);
        Card card = new Card(cardNumber, _monthID, _yearID,  cardCode);
        card.setName(FormUtils.getFieldText(_ccName));

        if (!card.validateCVC()) {
            AlertDialog ad = FormUtils.addLightDialog(R.string.ccInvalidCode, this);
            ad.show();
            return;
        }
        if (!card.validateCard()) {
            AlertDialog ad = FormUtils.addLightDialog(R.string.ccInvalidNumber, this);
            ad.show();
            return;
        }

        if (!card.validateExpiryDate()) {
            AlertDialog ad = FormUtils.addLightDialog(R.string.ccInvalidDate, this);
            ad.show();
            return;
        }

        Intent intent = new Intent(v.getContext(), CheckOutActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("serviceInfo", _si);
        bundle.putParcelable("account", _account);
        bundle.putParcelable("card", card);
        intent.putExtra("paymentInfoBundle", bundle);
        startActivityForResult(intent, 3);
    }

    public void useTestCard(View v) {
        Card card = new Card("4242-4242-4242-4242", 1, 2016, "123");
        Intent intent = new Intent(v.getContext(), CheckOutActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("serviceInfo", _si);
        bundle.putParcelable("account", _account);
        bundle.putParcelable("card", card);
        intent.putExtra("paymentInfoBundle", bundle);
        startActivityForResult(intent, 3);
    }

    public void showYearPicker(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.year_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    public void showMonthPicker(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.month_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.febId:
                _monthID = 2;
                _monthBtn.setText(R.string.feb);
                return true;
            case R.id.marId:
                _monthID = 3;
                _monthBtn.setText(R.string.mar);
                return true;
            case R.id.aprId:
                _monthID = 4;
                _monthBtn.setText(R.string.apr);
                return true;
            case R.id.mayId:
                _monthID = 5;
                _monthBtn.setText(R.string.may);
                return true;
            case R.id.junId:
                _monthID = 6;
                _monthBtn.setText(R.string.jun);
                return true;
            case R.id.julId:
                _monthID = 7;
                _monthBtn.setText(R.string.jul);
                return true;
            case R.id.augId:
                _monthID = 8;
                _monthBtn.setText(R.string.aug);
                return true;
            case R.id.sepId:
                _monthID = 9;
                _monthBtn.setText(R.string.sep);
                return true;
            case R.id.octId:
                _monthID = 10;
                _monthBtn.setText(R.string.oct);
                return true;
            case R.id.novId:
                _monthID = 11;
                _monthBtn.setText(R.string.nov);
                return true;
            case R.id.decId:
                _monthID = 12;
                _monthBtn.setText(R.string.dec);
                return true;
            case R.id.y2014Id:
                _yearID = 2014;
                _yearBtn.setText(R.string.y2014);
                return true;
            case R.id.y2015Id:
                _yearID = 2015;
                _yearBtn.setText(R.string.y2015);
                return true;
            case R.id.y2016Id:
                _yearID = 2016;
                _yearBtn.setText(R.string.y2016);
                return true;
            case R.id.y2017Id:
                _yearID = 2017;
                _yearBtn.setText(R.string.y2017);
                return true;
            case R.id.y2018Id:
                _yearID = 2018;
                _yearBtn.setText(R.string.y2018);
                return true;
            default:
                _monthID = 0;
                _monthBtn.setText(R.string.jan);
                return true;
        }
    }
}
