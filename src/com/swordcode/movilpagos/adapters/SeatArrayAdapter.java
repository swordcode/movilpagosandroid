package com.swordcode.movilpagos.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.models.Seat;

/**
 * Created by euclidesflores on 1/29/14.
 */
public class SeatArrayAdapter extends ArrayAdapter<String>
{
	private Seat[]				_seats;
	private String[]			_values;
	private final Context 		_context;
	private EventTicket			_event;


	public SeatArrayAdapter(Context context, int resource, EventTicket event, String[] values, Seat[] seats) {
		super(context, resource, values);
		_context = context;
		_values = values;
		_seats = seats;
		_event = event;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return super.getView(position, convertView, parent);
	}
}
