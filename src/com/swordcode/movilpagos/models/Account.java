package com.swordcode.movilpagos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

@ParseClassName("Accounts")
public class Account extends ParseObject implements Parcelable
{
	private static final long serialVersionUID = 1L;
	private String 		_objID;
	private String		_userID;
	private String		_accountID;
	private Number		_serviceID;
	private Number		_amountDue;
	private Date		_dueDate;
	private String		_customer;
	private String		_tempHolder;

    public Account() {
    }

	public Account(String objID, String userID, String accountID,
			Number serviceID, Number amountDue, Date dueDate,
			String customer, String tempHolder) {
		_objID = objID;
		_userID = userID;
		_accountID = accountID;
		_serviceID = serviceID;
		_amountDue = amountDue;
		_dueDate = dueDate;
		_customer = customer;
		_tempHolder = tempHolder;
	}

	public String getObjID() {
		return _objID;
	}

	public String getUserID() {
		return _userID;
	}

	public String getAccountID() {
		return _accountID;
	}

	public Number getServiceID() {
		return _serviceID;
	}

	public Number getAmountDue() {
		return _amountDue;
	}

	public Date getDueDate() {
		return _dueDate;
	}

	public String getCustomer() {
		return _customer;
	}

	public String getTempHolder() {
		return _tempHolder;
	}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_objID);
        parcel.writeString(_userID);
        parcel.writeString(_accountID);
        parcel.writeInt(_serviceID.intValue());
        parcel.writeDouble(_amountDue.doubleValue());
        parcel.writeLong(_dueDate.getTime());
        parcel.writeString(_customer);
    }

    public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator<Account>() {
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    private Account(Parcel in) {
        _objID = in.readString();
        _userID = in.readString();
        _accountID = in.readString();
        _serviceID = in.readInt();
        _amountDue = in.readDouble();
        _dueDate = new Date(in.readLong());
        _customer = in.readString();
    }
}
