package com.tabs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.swordcode.movilpagos.R;
import com.swordcode.movilpagos.SelectEventSeatActivity;
import com.swordcode.movilpagos.SelectMovieDatesActivity;
import com.swordcode.movilpagos.adapters.EventArrayAdapter;
import com.swordcode.movilpagos.models.EventTicket;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.swordcode.movilpagos.constants.Constants.EVENT_CONCERT;
import static com.swordcode.movilpagos.constants.Constants.EVENT_EVENT;
import static com.swordcode.movilpagos.constants.Constants.EVENT_MOVIE;

/**
 * Created by euclidesflores on 1/28/14.
 */
public class EventTab extends Fragment
{
	private int					_eventType;
	private EventTicket[]		_array;
	private String[]			_values;
	private ListView 			_eventList;
	private EventArrayAdapter 	_adapter;

	public EventTab() {
	}

	public EventTab(int eventType)
	{
		_eventType = eventType;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy mm:hh");
		Date date = new Date();
		try {
			date = sdf.parse("19/02/2014 20:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		switch(_eventType) {
			case EVENT_EVENT: {
				EventTicket t1 = new EventTicket("4", "Cirque du Soleil", EVENT_EVENT, date, "", "", 0.0, 0.0, 0);
				_array = new EventTicket[]{t1};
				break;
			}
			case EVENT_CONCERT: {
				EventTicket t1 =  new EventTicket("5", "Maroon 5 en Concierto", EVENT_CONCERT, date, "", "", 0.0, 0.0, 0);
				_array = new EventTicket[]{t1};
				break;
			}
			default: {
				EventTicket t1 = new EventTicket("1", "The Hobbit: The desolation of Smaug", EVENT_MOVIE, new Date(), "", "", 0.0, 0.0, 0);
				EventTicket t2 = new EventTicket("2", "Lone Survivor", EVENT_MOVIE, new Date(), "", "", 0.0, 0.0, 0);
				EventTicket t3 = new EventTicket("3", "The Secret Life of Walter Mitty", EVENT_MOVIE, new Date(), "", "", 0.0, 0.0, 0);
				_array = new EventTicket[]{t1, t2, t3};
			}
		}
		_values = new String[_array.length];
		for (int i=0;i < _array.length; i++) {
			_values[i] = _array[i].getEventName();
		}
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.events_tab_layout, container, false);
		_eventList = (ListView)rootView.findViewById(R.id.event_list);
		_adapter = new EventArrayAdapter(getActivity(), R.layout.event_row_layout, _values, _array) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater)EventTab.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView = inflater.inflate(R.layout.event_row_layout, parent, false);
				TextView textView = (TextView)rowView.findViewById(R.id.eventText);
				textView.setText(_values[position]);
				final EventTicket event = _array[position];
				Button btn = (Button)rowView.findViewById(R.id.eventBuyBtn);
				btn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						switch (event.getEventType().intValue()) {
							case EVENT_MOVIE:
								Intent intent1 = new Intent(EventTab.this.getActivity(), SelectMovieDatesActivity.class);
								Bundle bundle1 = new Bundle();
								bundle1.putParcelable("event", event);
								intent1.putExtra("eventInfoBundle", bundle1);
								EventTab.this.getActivity().startActivityForResult(intent1, 1);
								break;
							default:
								Intent intent = new Intent(EventTab.this.getActivity(), SelectEventSeatActivity.class);
								Bundle bundle = new Bundle();
								bundle.putParcelable("event", event);
								intent.putExtra("eventInfoBundle", bundle);
								EventTab.this.getActivity().startActivityForResult(intent, 1);
						}
					}
				});
				ImageView imageView = (ImageView)rowView.findViewById(R.id.eventImage);
				int eventID = Integer.parseInt(event.getEventID());
				switch (eventID) {
					case 1:
						imageView.setImageResource(R.drawable.thehobbit);
						break;
					case 2:
						imageView.setImageResource(R.drawable.lonesurvivorposter731f);
						break;
					case 3:
						imageView.setImageResource(R.drawable.waltermitty);
						break;
					case 4:
						imageView.setImageResource(R.drawable.circodelsol);
						break;
					case 5:
						imageView.setImageResource(R.drawable.maroon_5);
						break;
				}
				return rowView;
			}
		};
		_eventList.setAdapter(_adapter);
        return rootView;
    }

}
