package com.swordcode.movilpagos;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ServiceArrayAdapter extends ArrayAdapter<String> {
	private final Context 		_context;
	private final String[] 		_values;

	public ServiceArrayAdapter(Context context, String[] values) {
		super(context, R.layout.list_service, values);
		_context = context;
		_values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.list_service_layout, parent, false);
		TextView textView = (TextView)rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView)rowView.findViewById(R.id.logo);
		textView.setText(_values[position]);
		switch (position) {
			case 0: {
				imageView.setImageResource(R.drawable.gasnatural);
				String text = "<font color=#FFCC00>Gas</font><font color=#FF9500>Natural</font>";
				textView.setText(Html.fromHtml(text));
				break;
			}
			case 1: {
				imageView.setImageResource(R.drawable.idaan);
				textView.setTextColor(Color.parseColor("#007AFF"));
				break;
			}
			default: {
				imageView.setImageResource(R.drawable.ticket6);
			}
		}
		return rowView;
	}
}
