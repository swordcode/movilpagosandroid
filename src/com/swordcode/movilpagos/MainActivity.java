package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.models.Seat;
import com.swordcode.movilpagos.models.ServiceInfo;
import com.swordcode.movilpagos.models.Transaction;

import java.util.ArrayList;
import java.util.List;

import static com.swordcode.movilpagos.constants.Constants.ST_Utility;

public class MainActivity extends Activity {
    private ListView 					_serviceList;
    private List<ServiceInfo> 			_data;
    private ServiceArrayAdapter 		_adapter;
    private TextView 					_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        ParseObject.registerSubclass(ServiceInfo.class);
        ParseObject.registerSubclass(Account.class);
        ParseObject.registerSubclass(Transaction.class);
		ParseObject.registerSubclass(EventTicket.class);
		ParseObject.registerSubclass(Seat.class);
        Parse.initialize(this, "4Gck5iSGLQCpvA8rip17FI48JA4WNqDdeL0wQdi6", "f2cTG4YGSvZ50cYh71duBS9fgRQ4GsWh37VYJbx5");
        setContentView(R.layout.activity_main);
        _serviceList = (ListView) findViewById(R.id.service_list);
        _tv = new TextView(this);
        _tv.setText("Selecciona un servicio a pagar");
        _serviceList.addHeaderView(_tv, null, false);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
        setProgressBarIndeterminateVisibility(true);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                _data = new ArrayList<ServiceInfo>();
                String values[] = new String[objects == null ? 0 : objects.size()];
                int i = 0;
                if (objects != null) {
                    for (ParseObject o : objects) {
                        String objID = o.getObjectId();
                        Number serviceID = o.getNumber("serviceID");
                        Number serviceType = o.getNumber("ServiceType");
                        String serviceName = o.getString("ServiceName");
                        String description = o.getString("Description");
                        Number processingType = o.getNumber("ProcessingType");
                        boolean enabled = o.getBoolean("enabled");
                        ServiceInfo si =
                                new ServiceInfo(objID, serviceID, serviceType, serviceName,
                                        description, processingType, enabled);
                        _data.add(si);
                        values[i] = si.getDescription();
                        i++;
                    }
                }
                _adapter = new ServiceArrayAdapter(MainActivity.this, values);
                _serviceList.setAdapter(_adapter);
                _serviceList.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view,
                                            int position, long arg3) {
                        ParseUser currentUser = ParseUser.getCurrentUser();
                        int index = position - 1;
                        ServiceInfo si = _data.get(index);
                        int serviceType = si.getServiceType().intValue();
                        switch (serviceType) {
                            case ST_Utility: {
                                if (currentUser == null) {
                                    Intent intent = new Intent(view.getContext(), LoginActivity.class);
                                    intent.putExtra("selectedItem", position);
                                    startActivityForResult(intent, 0);
                                } else {
                                    Intent intent = new Intent(view.getContext(), SelectAccountActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("serviceInfo", si);
                                    intent.putExtra("serviceInfoBundle", bundle);
                                    startActivityForResult(intent, 1);
                                }
                                break;
                            }
                            default: {
                                Intent intent = new Intent(view.getContext(), TicketEventActivity.class);
                                startActivityForResult(intent, 1);
                            }
                        }
                    }
                });
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_history: {
				openPaymentHistory();
				return true;
			}
			case R.id.action_settings: {
				openPreferences();
				return true;
			}
			default: {
				return super.onOptionsItemSelected(item);
			}
		}
	}

	private void openPreferences() {
		Intent intent = new Intent(getApplicationContext(), PreferencesActivity.class);
		startActivityForResult(intent, 1);
	}

	private void openPaymentHistory() {
		Intent intent = new Intent(getApplicationContext(), PaymentHistoryActivity.class);
		Bundle bundle = new Bundle();
		ArrayList<ServiceInfo> list = new ArrayList<ServiceInfo>();
		for (ServiceInfo si : _data) {
			list.add(si);
		}
		bundle.putParcelableArrayList("serviceInfoList", list);
		intent.putExtra("serviceInfoBundle", bundle);
		startActivityForResult(intent, 1);
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

}
