package com.swordcode.movilpagos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

/**
 * Created by euclidesflores on 1/28/14.
 */
@ParseClassName("EventTicket")
public class EventTicket extends ParseObject implements Parcelable
{
    private String          _eventID;
    private String          _eventName;
    private Number          _eventType;
    private Date            _eventDate;
    private String          _eventSchedule;
    private String          _eventRoom;
    private Number          _amount;
    private Number          _amountEach;
    private Number          _seats;


    public EventTicket() {
    }

	public EventTicket(Number eventType, String eventID, String eventName) {
		_eventType = eventType;
		_eventID = eventID;
		_eventName = eventName;
	}

	public EventTicket(String eventID, String eventName, Number eventType, Date eventDate,
                       String eventSchedule, String eventRoom, Number amount, Number amountEach,
                       Number seats) {
        _eventID = eventID;
        _eventName = eventName;
        _eventType = eventType;
        _eventDate = eventDate;
        _eventSchedule = eventSchedule;
        _eventRoom = eventRoom;
        _amount = amount;
        _amountEach = amountEach;
        _seats = seats;
    }

    public String getEventID() {
        return _eventID;
    }

    public String getEventName() {
        return _eventName;
    }

    public Number getEventType() {
        return _eventType;
    }

    public Date getEventDate() {
        return _eventDate;
    }

    public String getEventSchedule() {
        return _eventSchedule;
    }

    public String getEventRoom() {
        return _eventRoom;
    }

    public Number getAmount() {
        return _amount;
    }

    public Number getAmountEach() {
        return _amountEach;
    }

    public Number getSeats() {
        return _seats;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(_eventID);
		parcel.writeString(_eventName);
		parcel.writeInt(_eventType.intValue());
		parcel.writeLong(_eventDate.getTime());
		parcel.writeString(_eventSchedule);
		parcel.writeString(_eventRoom);
		parcel.writeDouble(_amount.doubleValue());
		parcel.writeDouble(_amountEach.doubleValue());
		parcel.writeInt(_seats.intValue());
    }

    public static final Parcelable.Creator<EventTicket> CREATOR = new Parcelable.Creator<EventTicket>() {
        public EventTicket createFromParcel(Parcel in) {
            return new EventTicket(in);
        }

        public EventTicket[] newArray(int size) {
            return new EventTicket[size];
		}
    };

    private EventTicket(Parcel in) {
		_eventID = in.readString();
		_eventName = in.readString();
		_eventType = in.readInt();
		_eventDate = new Date(in.readLong());
		_eventSchedule = in.readString();
		_eventRoom = in.readString();
		_amount = in.readDouble();
		_amountEach = in.readDouble();
		_seats = in.readInt();
	}
}
