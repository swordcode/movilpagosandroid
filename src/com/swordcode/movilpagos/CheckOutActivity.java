package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.Stripe;
import com.android.TokenCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;
import com.swordcode.movilpagos.models.Account;
import com.swordcode.movilpagos.models.ServiceInfo;
import com.swordcode.movilpagos.models.Transaction;
import com.swordcode.movilpagos.utils.FormUtils;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.swordcode.movilpagos.constants.Constants.*;

/**
 * Created by euclidesflores on 1/24/14.
 */
public class CheckOutActivity extends Activity
{
    private TextView            _serviceName;
    private TextView            _accountNo;
    private TextView            _customer;
    private TextView            _paymentType;
    private TextView            _ccNumber;
    private TextView            _amountDue;
    private EditText            _amount;
    private Button              _commit;
    private ServiceInfo         _si;
    private Account             _account;
    private Card                _card;
    private NumberFormat        _format = NumberFormat.getCurrencyInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getBundleExtra("paymentInfoBundle");
        _si = (ServiceInfo)bundle.getParcelable("serviceInfo");
        _account = (Account)bundle.getParcelable("account");
        _card = (Card)bundle.getParcelable("card");
        setTitle(getApplicationContext().getString(R.string.checkoutActionHeader));
        setContentView(R.layout.checkout_layout);
        _serviceName = (TextView)findViewById(R.id.checkoutServiceText);
        _serviceName.setText(_si.getDescription());
        _accountNo = (TextView)findViewById(R.id.checkoutAccountNoText);
        _accountNo.setText(_account.getAccountID());
        _customer = (TextView)findViewById(R.id.checkoutCustomerText);
        _customer.setText(_account.getCustomer());
        _paymentType = (TextView)findViewById(R.id.checkoutPaymentTypeText);
        _paymentType.setText(R.string.checkoutPaymentTypeTxt);
        _ccNumber = (TextView)findViewById(R.id.checkoutCardNoText);
        String ccStr = String.format("%s****%s", _card.getType(), _card.getLast4());
        _ccNumber.setText(ccStr);
        _amountDue = (TextView)findViewById(R.id.checkoutAmountDueText);
        Number amountDue = _account.getAmountDue();
        _amountDue.setText(_format.format(amountDue));
        _amount = (EditText)findViewById(R.id.checkoutAmountText);
        _commit = (Button)findViewById(R.id.checkoutCommitBtn);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void checkout(final View v) {
        final Context ctx = getApplicationContext();
        final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
        pd.setMessage(ctx.getString(R.string.checkoutCardValidating));
        AlertDialog dialog = FormUtils.addLightDialog(R.string.checkoutAmountEmptyBody, this);
        if (FormUtils.isEditFieldEmpty(_amount)) {
            dialog.setTitle(R.string.checkoutAmountEmptyTitle);
            dialog.show();
            return;
        }

        String amountStr = FormUtils.getFieldText(_amount);
        final double amount = Double.valueOf(amountStr);
        if (amount < 5.0) {
            dialog.setTitle(R.string.checkoutAmountInvalidTitle);
            dialog.setMessage(ctx.getString(R.string.checkoutAmountInvalidBody));
            dialog.show();
            return;
        }

        pd.show();

        try {
            Stripe stripe = new Stripe("pk_test_ityaqG2Yxz4ojcKg3BryUExc");
            stripe.createToken(_card, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    Toast.makeText(ctx, error.getMessage(),
                            Toast.LENGTH_LONG
                    ).show();
                }

                @Override
                public void onSuccess(Token token) {
                    Toast.makeText(ctx, R.string.checkoutCardValidationFinished,
                            Toast.LENGTH_LONG
                    ).show();
                    postStripeToken(token.getId(), (Number)amount, v);
                }
            });
        } catch (AuthenticationException e) {
            e.printStackTrace();
        } finally {
            pd.dismiss();
        }

    }

    public void postStripeToken(String tokenID, Number amount, final View v) {
        final Context ctx = getApplicationContext();
        final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
        pd.setMessage(ctx.getString(R.string.checkoutAuthorizing));
        pd.show();
        Map<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("serviceID", _si.getServiceID());
        dictionary.put("userID", ParseUser.getCurrentUser().getObjectId());
        dictionary.put("paymentDate", new Date());
        dictionary.put("amount", amount);
        dictionary.put("balance", _account.getAmountDue());
        dictionary.put("accountID", _account.getAccountID());
        dictionary.put("cardToken", tokenID);
        ParseCloud.callFunctionInBackground("checkoutService", dictionary, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                pd.dismiss();
                if (e != null) {
                    Toast.makeText(ctx, e.getMessage(),
                            Toast.LENGTH_LONG
                    ).show();
                } else {
                    ParseObject po = (ParseObject)o;
                    String trxID = po.getObjectId();
                    Date paymentDate = po.getDate("payment_date");
                    Number amount = po.getNumber("amount");
                    Number balance = po.getNumber("balance");
                    String cardType = po.getString("cardType");
                    String userID = po.getString("userID");
                    String last4 = po.getString("last4");
                    String accountID = po.getString("accountID");
                    String stripePaymentID = po.getString("stripePaymentID");
                    Number serviceID = po.getNumber("serviceID");
                    Transaction trx = new Transaction(trxID, userID, accountID, paymentDate,
                                        amount, balance, serviceID, stripePaymentID, last4,
                                        cardType, 0, _si.getServiceName()
                                      );
                    Toast.makeText(ctx, ctx.getText(R.string.checkoutAuthorizationFinished),
                            Toast.LENGTH_LONG
                    ).show();
                    Intent intent = new Intent(v.getContext(), CheckoutCompleteActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("trx", trx);
                    intent.putExtra("trxBundle", bundle);
					intent.putExtra("requestCode", ACTIVITY_CODE_DEF);
                    startActivityForResult(intent, ACTIVITY_CODE_DEF);
                }
            }
        });
    }
}
