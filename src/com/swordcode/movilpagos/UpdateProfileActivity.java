package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.swordcode.movilpagos.utils.FormUtils;

/**
 * Created by euclidesflores on 2/11/14.
 */
public class UpdateProfileActivity extends Activity
{
	private TextView						_header;
	private EditText						_userName;
	private EditText						_fullName;
	private EditText						_email;
	private Button							_update;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_profile_layout);
		setTitle("Editar perfil");
		_header = (TextView)findViewById(R.id.updateProfileHeader);
		String headerStr = getApplicationContext().getString(R.string.updateProfileHeader);
		ParseUser currentUser = ParseUser.getCurrentUser();
		headerStr = String.format(headerStr, currentUser.getUsername());
		_header.setText(headerStr);
		_update = (Button)findViewById(R.id.updateProfileBtn);
		_update.setBackgroundColor(Color.parseColor("#FF9500"));
		_update.setTextColor(Color.WHITE);

		_userName = (EditText)findViewById(R.id.userName);
		_userName.setText(currentUser.getUsername());
		_fullName = (EditText)findViewById(R.id.signUpfullName);
		_fullName.setText(currentUser.getString("additional"));
		_email = (EditText)findViewById(R.id.signUpEmail);
		_email.setText(currentUser.getEmail());
	}

	public void updateProfile(View view) {
		final Context ctx = getApplicationContext();
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final AlertDialog errorDialog = FormUtils.addLightDialog(R.string.updatePasswordMismatch, this);
		errorDialog.setTitle("Edición de perfil");

		if (FormUtils.isEditFieldEmpty(_userName) && FormUtils.isEditFieldEmpty(_fullName) && FormUtils.isEditFieldEmpty(_email)) {
			errorDialog.setMessage("Ningún campo para actualizar");
			errorDialog.show();
			return;
		}

		if (!FormUtils.validateEmailField(_email)) {
			errorDialog.setMessage(ctx.getString(R.string.loginMssgEmailEmpty));
			errorDialog.show();
			return;
		}

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		input.setHint("Su contraseña");
		builder.setTitle("Contraseña");
		builder.setMessage("Ingrese su contraseña para realizar los cambios");
		builder.setView(input);

		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled
			}
		});

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				final ProgressDialog pd = new ProgressDialog(UpdateProfileActivity.this, ProgressDialog.STYLE_SPINNER);
				pd.setMessage("Actualizando perfil");
				pd.show();
				ParseUser.logInInBackground(currentUser.getUsername(), FormUtils.getFieldText(input),
						new LogInCallback() {
							@Override
							public void done(ParseUser parseUser, ParseException e) {
								boolean update = false;
								if (e != null) {
									pd.dismiss();
									String mssg = FormUtils.getErrorMssg(e.getCode(), ctx);
									Toast.makeText(ctx, mssg, Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(ctx, "Perfil actualizado", Toast.LENGTH_SHORT).show();
									if (!FormUtils.isEditFieldEmpty(_userName) &&
											!FormUtils.getFieldText(_userName).equals(currentUser.getUsername())) {
										currentUser.setUsername(FormUtils.getFieldText(_userName));
										update = true;
									}

									if (!FormUtils.isEditFieldEmpty(_fullName) &&
											!FormUtils.getFieldText(_fullName).equals(currentUser.getString("additional"))) {
										currentUser.put("additional", FormUtils.getFieldText(_fullName));
										update = true;
									}

									if (!FormUtils.isEditFieldEmpty(_email) &&
											!FormUtils.getFieldText(_email).equals(currentUser.getEmail())) {
										currentUser.setEmail(FormUtils.getFieldText(_email));
										update = true;
									}

									if (update) {
										currentUser.saveInBackground(new SaveCallback() {
											@Override
											public void done(ParseException e) {
												try {
													ParseUser.logIn(currentUser.getUsername(), FormUtils.getFieldText(input));
												} catch (ParseException e1) {
													e1.printStackTrace();
												}
												pd.dismiss();
												UpdateProfileActivity.this.finish();
											}
										});
									} else {
										pd.dismiss();
										UpdateProfileActivity.this.finish();
									}
								}
							}
						}
				);

			}
		});

		final AlertDialog dialog = builder.create();
		input.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Button btn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				btn.setEnabled(input.length() > 0);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			}
		});
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
