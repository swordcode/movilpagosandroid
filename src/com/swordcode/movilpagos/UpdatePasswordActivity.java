package com.swordcode.movilpagos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.swordcode.movilpagos.utils.FormUtils;

/**
 * Created by euclidesflores on 2/11/14.
 */
public class UpdatePasswordActivity extends Activity
{
	private EditText						_oldPassword;
	private EditText						_newPassword;
	private EditText						_confirmPassword;
	private Button 							_update;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Cambiar contraseña");
		setContentView(R.layout.activity_update_password_layout);
		_oldPassword = (EditText)findViewById(R.id.oldPassword);
		_newPassword = (EditText)findViewById(R.id.newPassword);
		_confirmPassword = (EditText)findViewById(R.id.confirmPassword);
		_update = (Button)findViewById(R.id.updatePasswordBtn);
		_update.setBackgroundColor(Color.parseColor("#FF9500"));
		_update.setTextColor(Color.WHITE);
		TextView header = (TextView)findViewById(R.id.updatePasswordHeader);
		String headerStr = getApplicationContext().getString(R.string.updatePasswordHeader);
		ParseUser currentUser = ParseUser.getCurrentUser();
		headerStr = String.format(headerStr, currentUser.getUsername());
		header.setText(headerStr);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void updatePassword(View v) {
		final Context ctx = getApplicationContext();
		String oldPwd = _oldPassword.getText().toString();
		String newPwd = _newPassword.getText().toString();
		String cfmPwd = _confirmPassword.getText().toString();
		final AlertDialog errorDialog = FormUtils.addLightDialog(R.string.updatePasswordMismatch, this);
		errorDialog.setTitle("Cambio de contraseña");
		if (oldPwd.length() < 1 || newPwd.length() < 1 || cfmPwd.length() < 1) {
			errorDialog.show();
			return;
		}

		if (newPwd.length() < 7 || cfmPwd.length() < 7) {
			errorDialog.setMessage(ctx.getText(R.string.updatePasswordShorterThanExpected));
			errorDialog.show();
			return;
		}

		if (!newPwd.equals(cfmPwd)) {
			errorDialog.setMessage(ctx.getText(R.string.updatePasswordMismatchNewAndConfirm));
			errorDialog.show();
			return;
		}

		final ProgressDialog pd = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
		final ParseUser currentUser = ParseUser.getCurrentUser();
		pd.setMessage(ctx.getString(R.string.updatePasswordProgess));
		pd.show();
		ParseUser.logInInBackground(currentUser.getUsername(), FormUtils.getFieldText(_oldPassword),
			new LogInCallback() {
				@Override
				public void done(ParseUser parseUser, ParseException e) {
					pd.dismiss();
					if (e != null) {
						errorDialog.setMessage(ctx.getText(R.string.updatePasswordInvalidCurrentPassword));
						errorDialog.show();
					} else {
						Toast.makeText(ctx, "Contraseña actualizada", Toast.LENGTH_SHORT).show();
						currentUser.setPassword(FormUtils.getFieldText(_newPassword));
						currentUser.saveInBackground();
						UpdatePasswordActivity.this.finish();
					}
				}
			}
		);
	}
}
