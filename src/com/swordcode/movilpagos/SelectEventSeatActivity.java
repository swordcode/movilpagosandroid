package com.swordcode.movilpagos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.swordcode.movilpagos.adapters.SeatArrayAdapter;
import com.swordcode.movilpagos.models.EventTicket;
import com.swordcode.movilpagos.models.Seat;

import java.text.NumberFormat;

/**
 * Created by euclidesflores on 1/29/14.
 */
public class SelectEventSeatActivity extends Activity
{
	private ListView						_list;
	private Seat[]							_data;
	private EventTicket						_event;
	private SeatArrayAdapter				_adapter;
	private String[]						_values;
	private NumberFormat					_format = NumberFormat.getCurrencyInstance();

	public SelectEventSeatActivity() {
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_event_seat_activity);
		_list = (ListView)findViewById(R.id.seat_list);
		Bundle bundle = getIntent().getBundleExtra("eventInfoBundle");
		_event = (EventTicket)bundle.getParcelable("event");
		setTitle(_event.getEventName());
		String eID = _event.getEventID();
		Seat s1 = new Seat(2, 190.0f, eID, 4, 24);
		Seat s2 = new Seat(2, 90.0f, eID, 2, 6);
		Seat s3 = new Seat(3, 90.0f, eID, 6, 2);
		Seat s4 = new Seat(3, 50.0f, eID, 10, 19);
		Seat s5 = new Seat(1, 20.0f, eID, 3, 20);
		_data = new Seat[]{s1, s2, s3, s4, s5};
		_values = new String[_data.length];
		_values[0] = s1.getSection().toString();
		_values[1] = s2.getSection().toString();
		_values[2] = s3.getSection().toString();
		_values[3] = s4.getSection().toString();
		_values[4] = s5.getSection().toString();
		_adapter = new SeatArrayAdapter(getApplicationContext(),
				R.layout.select_seat_row_layout, _event, _values, _data
		) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater inflater = (LayoutInflater)SelectEventSeatActivity.this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rowView = inflater.inflate(R.layout.select_seat_row_layout, parent, false);
				TextView textView = (TextView)rowView.findViewById(R.id.sectionTxt);
				textView.setText(_values[position]);
				Seat seat = _data[position];
				TextView row = (TextView)rowView.findViewById(R.id.rowTxt);
				row.setText(seat.getRow().toString());
				TextView qty = (TextView)rowView.findViewById(R.id.qtyTxt);
				qty.setText(seat.getQty().toString());
				TextView price = (TextView)rowView.findViewById(R.id.priceTxt);
				price.setText(_format.format(seat.getPrice()));
				return rowView;
			}
		};

		_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				Intent intent = new Intent(SelectEventSeatActivity.this.getApplicationContext(), EventPrecheckoutActivity.class);
				Bundle bundle = new Bundle();
				bundle.putParcelable("event", _event);
				bundle.putParcelable("seat", _data[i-1]);
				intent.putExtra("eventAndSeatBundle", bundle);
				startActivityForResult(intent, 5);
			}
		});
		_list.addHeaderView(createHeader(), null, false);
		_list.setAdapter(_adapter);
	}

	private View createHeader() {
		LayoutInflater inflater = (LayoutInflater)SelectEventSeatActivity.this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View headerView = inflater.inflate(R.layout.select_seat_map_row_layout, null);
		return headerView;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home: {
				finish();
				return true;
			}
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}