package com.swordcode.movilpagos.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Services")
public class ServiceInfo extends ParseObject implements Parcelable
{
	private String 		_objID;
	private Number  	_serviceID;
	private Number 		_serviceType;
	private String		_serviceName;
	private String 		_description;
	private Number		_processingType;
	private boolean		_enabled;

	public ServiceInfo() {
	}

	public ServiceInfo(String objID, Number serviceID, Number serviceType,
			String serviceName, String description, Number processingType,
			boolean enabled) {
		_objID = objID;
		_serviceID = serviceID;
		_serviceType = serviceType;
		_serviceName = serviceName;
		_description = description;
		_processingType = processingType;
		_enabled = enabled;
	}

	public Number getServiceID() {
		return _serviceID;
	}

	public Number getServiceType() {
		return _serviceType;
	}

	public String getServiceName() {
		return _serviceName;
	}

	public String getDescription() {
		return _description;
	}

	public Number geProcessingType() {
		return _processingType;
	}

	public boolean isEnabled() {
		return _enabled;
	}

	public String getObjID() {
		return _objID;
	}

//	Parceable methods

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(_objID);
		parcel.writeInt(_serviceID.intValue());
		parcel.writeInt(_serviceType.intValue());
		parcel.writeString(_serviceName);
		parcel.writeString(_description);
		parcel.writeInt(_processingType.intValue());
		parcel.writeInt(_enabled ? 0 : 1);
	}

	public static final Parcelable.Creator<ServiceInfo> CREATOR = new Parcelable.Creator<ServiceInfo>() {
		public ServiceInfo createFromParcel(Parcel in) {
			return new ServiceInfo(in);
		}

		public ServiceInfo[] newArray(int size) {
			return new ServiceInfo[size];
		}
	};

	private ServiceInfo(Parcel in) {
		_objID = in.readString();
		_serviceID = in.readInt();
		_serviceType = in.readInt();
		_serviceName = in.readString();
		_description = in.readString();
		_processingType = in.readInt();
		_enabled = in.readInt() == 0 ? true : false;
	}
}
